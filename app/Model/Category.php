<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Category extends Model
{
    use Notifiable, Uuid;

    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'category';

    protected $fillable = [
        'type','name','title','description'
    ];

    public function productCategory()
    {
        return $this->hasMany(ProductCategory::class);
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'product_type';

    protected $fillable = [
        'name','title','description'
    ];

    public function product()
    {
        return $this->hasMany(Product::class);
    }
}

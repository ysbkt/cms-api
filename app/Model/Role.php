<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    use Notifiable, Uuid;

    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'role';

    protected $fillable = [
        'id','nama','status','description','created_by','updated_by','created_ip','updated_ip'
    ];

    public function userRole()
    {
        return $this->hasMany(UserRole::class);
    }

    public function checkRole($role)
    {
        $data   = DB::table('role')
        ->where('nama','=','?')
        ->get();

        return $data;
    }
}

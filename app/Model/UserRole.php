<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserRole extends Model
{
    use Notifiable, Uuid;

    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'user_role';

    protected $fillable = [
        'user_id','role_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}

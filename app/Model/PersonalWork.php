<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PersonalWork extends Model
{
    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'personal_work';

    protected $fillable = [
        'personal_id','company_id','title','position','company','description','address','start_date','end_date','portfolio_link','reference_name','reference_telp'
    ];

    public function personal()
    {
        return $this->belongsTo(Personal::class);
    }
}

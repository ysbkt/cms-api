<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Account extends Model
{
    use Notifiable, Uuid;

    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'account';

    protected $fillable = [
        'name', 'description'
    ];

    public function userAccount()
    {
        return $this->hasMany(UserAccount::class);
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'provider';

    protected $fillable = [
        'name','title'
    ];

    public function product()
    {
        return $this->hasMany(Product::class);
    }
}

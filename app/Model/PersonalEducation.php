<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PersonalEducation extends Model
{
    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'personal_education';

    protected $fillable = [
        'personal_id','degree','school','accreditation','description','address','grade','start_date','end_date'
    ];

    public function personal()
    {
        return $this->belongsTo(Personal::class);
    }
}

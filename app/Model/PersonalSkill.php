<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PersonalSkill extends Model
{
    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'personal_skill';

    protected $fillable = [
        'personal_id','title','skill','description','level','start_date','end_date'
    ];

    public function personal()
    {
        return $this->belongsTo(Personal::class);
    }
}

<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, Uuid;

    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password','salt'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'salt','created_at','modified_at','created_by','modified_by','created_ip','modified_ip'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userAccount()
    {
        return $this->hasMany(UserAccount::class);
    }

    public function userPasswordHistory()
    {
        return $this->hasMany(UserPasswordHistory::class);
    }

    public function userTokenKey()
    {
        return $this->hasMany(UserTokenKey::class);
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PersonalAddress extends Model
{
    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'personal_address';

    protected $fillable = [
        'personal_id','name','address1','address2','address3','country_id','province_id','city_id','sub_district_id','village_id','post_code','telp','handphone'
    ];

    protected $hidden = [
        'address1','address2','address3','telp','handphone'
    ];

    public function personal()
    {
        return $this->belongsTo(Personal::class);
    }
}

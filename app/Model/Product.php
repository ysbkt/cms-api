<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'product';

    protected $fillable = [
        'product_type_id','provider_id','vendor_id','title','name','type'
    ];

    public function productCategory()
    {
        return $this->hasMany(ProductCategory::class);
    }

    public function productType()
    {
        return $this->belongsTo(ProductType::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }
}

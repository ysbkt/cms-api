<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PersonalCompany extends Model
{
    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'personal_company';

    protected $fillable = [
        'type','personal_id','company_id'
    ];

    public function personal()
    {
        return $this->belongsTo(Personal::class);
    }
}

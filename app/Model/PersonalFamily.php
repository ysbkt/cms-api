<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PersonalFamily extends Model
{
    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'personal_family';

    protected $fillable = [
        'personal_id','identity_type','relationship','name','first_name','middle_name','last_name','gender','marital_status','religion','birth_date','place_of_birth','education','job','description'
    ];

    protected $hidden = [
        'identity_value'
    ];

    public function personal()
    {
        return $this->belongsTo(Personal::class);
    }
}

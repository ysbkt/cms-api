<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'product_category';

    protected $fillable = [
        'product_id','category_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}

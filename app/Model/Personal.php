<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Personal extends Model
{
    use Notifiable, Uuid;

    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'personal';

    protected $fillable = [
        'code','name','first_name','middle_name','las_name','nickname','gender','marial_status','religion','birth_date','place_of_birth'
    ];

    protected $hidden = [
        'birth_date'
    ];

    public function personalAddress()
    {
        return $this->hasMany(PersonalAddress::class);
    }

    public function personalCompany()
    {
        return $this->hasMany(PersonalCompany::class);
    }

    public function personalEducation()
    {
        return $this->hasMany(PersonalEducation::class);
    }

    public function personalFamily()
    {
        return $this->hasMany(PersonalFamily::class);
    }

    public function personalSkill()
    {
        return $this->hasMany(PersonalSkill::class);
    }

    public function personalUser()
    {
        return $this->hasMany(PersonalUser::class);
    }

    public function personalWork()
    {
        return $this->hasMany(PersonalWork::class);
    }
}

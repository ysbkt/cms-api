<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserPasswordHistory extends Model
{
    use Notifiable, Uuid;

    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'user_password_history';

    protected $fillable = [
        'user_id', 'enc', 'password', 'salt'
    ];

    protected $hidden = [
        'enc','password','salt'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

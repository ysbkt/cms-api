<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PersonalUser extends Model
{
    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'personal_user';

    protected $fillable = [
        'type','personal_id','user_id'
    ];

    public function personal()
    {
        return $this->belongsTo(Personal::class);
    }
}

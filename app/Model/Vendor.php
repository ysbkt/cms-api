<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Vendor extends Model
{
    use Notifiable, Uuid;

    public $incrementing    = false;
    protected $primary      = 'id';
    protected $table        = 'vendor';

    protected $fillable = [
        'code','name','title','description'
    ];

    public function product()
    {
        return $this->hasMany(Product::class);
    }
}

<?php

namespace App\Http\Requests;

use App\Model\Role;
use App\Rules\Nama;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class RoleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'          => 'required|string|regex:/^[a-zA-Z\s]+$/',
            'description'   => 'string',
            'status'        => 'integer|max:1'
        ];
    }
}

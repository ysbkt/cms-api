<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_education', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('personal_id');
            $table->string('degree', 50);
            $table->string('school', 50);
            $table->string('accreditaion', 10);
            $table->text('description')->nullable();
            $table->string('address');
            $table->string('grade', 5);
            $table->date('start_date')->index();
            $table->date('end_date')->index();
            $table->integer('status')->unsigned()->index();
            $table->timestamps();
            $table->string('created_by', 50)->index();
            $table->string('updated_by', 50)->nullable()->index();
            $table->ipAddress('created_ip');
            $table->ipAddress('updated_ip')->nullable();

            $table->foreign('personal_id')->references('id')->on('personal')->onUpdate('cascade')->onDelete('cascade');
        });

        /* Schema::hasTable('personal_education', function ( $table) {
            $table->foreign('personal_id')->references('id')->on('personal')->onDelete('cascade');
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_education');
    }
}

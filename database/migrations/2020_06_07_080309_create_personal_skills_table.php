<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_skill', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('personal_id');
            $table->string('title');
            $table->string('skill', 100);
            $table->text('description');
            $table->string('level', 20);
            $table->date('start_date')->index();
            $table->date('end_date')->index();
            $table->integer('status')->unsigned();
            $table->timestamps();
            $table->string('created_by', 50)->index();
            $table->string('updated_by', 50)->nullable()->index();
            $table->ipAddress('created_ip');
            $table->ipAddress('updated_ip')->nullable();

            $table->foreign('personal_id')->references('id')->on('personal')->onUpdate('cascade')->onDelete('cascade');
        });

        /* Schema::hasTable('personal_skill', function ( $table) {
            $table->foreign('personal_id')->references('id')->on('personal')->onDelete('cascade');
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_skill');
    }
}

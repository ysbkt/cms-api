<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_company', function (Blueprint $table) {
            $table->uuid('id')->primary();
            // type dan personal tidak boleh sama
            // type dan comapny tidak boleh sama
            $table->string('type')->unique()->index();
            $table->uuid('personal_id')->unique();
            $table->uuid('company_id')->unique()->nullable();
            $table->integer('status')->unsigned()->index();
            $table->timestamps();
            $table->string('created_by', 50)->index();
            $table->string('updated_by', 50)->nullable()->index();
            $table->ipAddress('created_ip');
            $table->ipAddress('updated_ip')->nullable();

            $table->foreign('personal_id')->references('id')->on('personal')->onUpdate('cascade')->onDelete('cascade');
        });

        /* Schema::hasTable('personal_company', function ( $table) {
            $table->foreign('personal_id')->references('id')->on('personal')->onDelete('cascade');
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_company');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_category', function (Blueprint $table) {
            $table->uuid('id')->primary();
            // product_id, category_id tidak boleh sama
            $table->uuid('product_id')->unique();
            $table->uuid('category_id')->unique();
            $table->integer('status')->unsigned();
            $table->timestamps();
            $table->string('created_by', 50)->index();
            $table->string('updated_by', 50)->nullable()->index();
            $table->ipAddress('created_ip');
            $table->ipAddress('updated_ip')->nullable();

            $table->foreign('product_id')->references('id')->on('product')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
        });

        /* Schema::hasTable('product_category', function ( $table) {
            $table->foreign('product_id')->references('id')->on('product')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_category');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_address', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('personal_id');
            $table->string('name', 20)->index();
            $table->string('address1', 50);
            $table->string('address2', 50);
            $table->string('address3', 50);
            $table->string('country_id', 50)->index();
            $table->string('province_id', 50)->index();
            $table->string('city_id', 50)->index();
            $table->string('sub_district_id', 50)->index();
            $table->string('village_id', 50)->index();
            $table->string('post_code', 50)->index();
            $table->string('telp', 20);
            $table->string('handphone', 20);
            $table->integer('status')->unsigned()->index();
            $table->timestamps();
            $table->string('created_by', 50)->index();
            $table->string('updated_by', 50)->nullable()->index();
            $table->ipAddress('created_ip');
            $table->ipAddress('updated_ip')->nullable();

            $table->foreign('personal_id')->references('id')->on('personal')->onDelete('cascade');
        });

        /* Schema::hasTable('personal_address', function ( $table) {
            $table->foreign('personal_id')->references('id')->on('personal')->onDelete('cascade');
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_address');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_family', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('personal_id');
            $table->string('identity_type', 20);
            $table->text('identity_value');
            $table->string('relationship', 50);
            $table->string('name', 50);
            $table->string('first_name', 50)->index();
            $table->string('middle_name', 50);
            $table->string('last_name', 50)->index();
            $table->string('gender', 1)->index();
            $table->string('marital_status', 1)->index();
            $table->string('religion', 20)->index();
            $table->date('birth_date');
            $table->string('place_of_birth', 50);
            $table->string('education', 50)->index();
            $table->string('job')->nullable();
            $table->text('description')->nullable();
            $table->integer('status')->unsigned()->index();
            $table->timestamps();
            $table->string('created_by', 50)->index();
            $table->string('updated_by', 50)->nullable()->index();
            $table->ipAddress('created_ip');
            $table->ipAddress('updated_ip')->nullable();

            $table->foreign('personal_id')->references('id')->on('personal')->onUpdate('cascade')->onDelete('cascade');
        });

        /* Schema::hasTable('personal_family', function ( $table) {
            $table->foreign('personal_id')->references('id')->on('personal')->onDelete('cascade');
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_family');
    }
}

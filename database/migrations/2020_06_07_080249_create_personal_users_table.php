<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_user', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('type')->unique()->index();
            $table->uuid('personal_id')->unique();
            $table->uuid('user_id')->unique();
            $table->integer('status')->unsigned()->index();
            $table->timestamps();
            $table->string('created_by', 50)->index();
            $table->string('updated_by', 50)->nullable()->index();
            $table->ipAddress('created_ip');
            $table->ipAddress('updated_ip')->nullable();

            $table->foreign('personal_id')->references('id')->on('personal')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
        });

        /* Schema::hasTable('personal_user', function ( $table) {
            $table->foreign('personal_id')->references('id')->on('personal')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_user');
    }
}
